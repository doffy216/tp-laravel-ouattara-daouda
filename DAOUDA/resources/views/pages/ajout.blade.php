@extends('layouts.main');
@section('content');
<div class="col-md-8">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Ajout</h4>
        <p class="card-category">Ajouter un pays</p>
      </div>
      <div class="card-body">
        <form action="{{route('pays.store')}}" method="POST">
            @method("POST")
            @csrf
          <div class="row">
            <div class="col-md-5">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Nom</label>
                <input type="text" class="form-control" name="Nom">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Description</label>
                <input type="text" class="form-control" name="Description">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Code</label>
                <input type="text" class="form-control" name="Code">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Continent</label>
                <input type="text" class="form-control" name="Continent">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Population</label>
                <input type="text" class="form-control" name="Population">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Capitale</label>
                <input type="text" class="form-control" name="Capitale">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating" style="position: relative;">Monnaie</label>
                <select name="Monnaie" id="" style="position: relative; width: 150px;top: 10px;z-index:0px;border-bottom:1px solid grey;border-top:0px solid grey;border-left:0px solid grey;border-right:0px solid grey;">
                    <option value="XOF">XOF</option>
                    <option value="EUR">EUR</option>
                    <option value="DOLLAR">DOLLAR</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating"  style="position: relative;">Langue</label>
                <select name="Langue" id=""  style="position: relative; width: 150px;top: 10px;z-index:0px;border-bottom:1px solid grey;border-top:0px solid grey;border-left:0px solid grey;border-right:0px solid grey;">
                    <option value="FR">FR</option>
                    <option value="AR">AR</option>
                    <option value="EN">EN</option>
                    <option value="ES">ES</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Superficie</label>
                <input type="text" class="form-control" name="Superficie" style="position: relative; width: 150px;top: 19px;z-index:0px;">
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group bmd-form-group">
              <label class="bmd-label-floating">Est laïque</label>
              <input type="text" class="form-control" name="Bool">
            </div>
          </div>
        </div>
          <button type="submit" class="btn btn-primary pull-right">Ajouter</button>
          <div class="clearfix"></div>
        </form>
      </div>
    </div>
</div>
@endsection

<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo"><a href="#" class="simple-text logo-normal">
        LARAVEL
      </a></div>
    <div class="sidebar-wrapper ps-container ps-theme-default" data-ps-id="c89adc59-e421-0b6f-cc15-22b692dbcb8c">
      <ul class="nav">
        <li class="nav-item active">
          <a class="nav-link" href="{{url('/')}}">
            <i class="material-icons">dashboard</i>
            <p>Tableau de bord</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{url('/pays.index')}}">
            <i class="material-icons">person</i>
            <p>Liste des pays</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{url('/pays.ajout')}}">
            <i class="material-icons">content_paste</i>
            <p>Ajout Pays</p>
          </a>
        </li>
        <li class="nav-item active-pro ">
          <a class="nav-link" href="#">
            <i class="material-icons">unarchive</i>
            <p>DOFFY</p>
          </a>
        </li>
      </ul>
    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
<div class="sidebar-background" style="background-image: url(../assets/img/sidebar-1.jpg) "></div></div>

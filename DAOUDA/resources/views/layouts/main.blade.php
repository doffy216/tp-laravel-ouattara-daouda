<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Material Dashboard by Creative Tim
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!-- Code css !-->
    @include('includes.css')
  <!-- Code css !-->
</head>

<body class="">

  <div class="wrapper ">
    <!-- sidebar !-->
        @include('includes.sidebar')
    <!-- sidebar !-->

    <div class="main-panel">
      <!-- Navbar -->
      @include('includes.navbar')
      <!-- End Navbar -->

      <!-- content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    @yield('content')
                </div>

            </div>
        </div>
      @include('includes.footer')
    </div>
    @include('includes.js')
</body>

</html>

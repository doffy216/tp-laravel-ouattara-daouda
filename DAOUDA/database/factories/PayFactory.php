<?php

namespace Database\Factories;

use App\Models\Pay;
use Illuminate\Database\Eloquent\Factories\Factory;

class PayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pay::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle' => $this->faker->country,
            'description' => $this->faker->sentence,
            'code_indicatif' => $this->faker->countryCode,
            'continent' => $this->faker->randomElement(['Afrique','Asie','Europe','Amerique']),
            'population' => $this->faker->randomElement([100000,30000,500,700000,20]),
            'capitale' => $this->faker->city,
            'monnaie' => $this->faker->randomElement(['XOF','EUR','DOLLARD']),
            'langue' => $this->faker->randomElement(['FR','EN','AR','ES']),
            'superficie' => $this->faker->randomElement([100,3000,5000,700,200]),
            'est_laique' => $this->faker->randomElement([true,false])
        ];
    }
}
